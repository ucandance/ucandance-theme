<?php

return [
    'title'       => 'UCanDance',
    'name'        => 'UCanDance Theme',
    'description' => 'Custom UCanDance theme made by Finn LeSueur'
];
