function initBugReporter() {
	// console.log("Initalising Bug Reporter");
	var bugReporter = document.querySelector(".bug-reporter");
	var bugReporterButton = document.querySelector(".bug-reporter-button");
	// console.log(bugReporter, bugReporterButton);

	// System Info
	var sysinfo = window.navigator.userAgent + " (" + window.screen.width + "px by " + window.screen.height + "px)";
	bugReporter.querySelector("textarea[name='sysinfo']").value = sysinfo;

	bugReporterButton.addEventListener("click", function() {
		bugReporter.classList.add("open");
		bugReporter.querySelector("textarea[name='report']").focus();
	});

	bugReporter.addEventListener("click", function(e) {
		if (bugReporter !== e.target) return;

		bugReporter.classList.remove("open");
		setTimeout(function() {
			bugReporter.querySelector(".success").classList.add("hidden");
			bugReporter.querySelector("form.ajax").classList.remove("hidden");
		}, 300);
	});
}
window.addEventListener("load", initBugReporter);
// window.addEventListener("load", initOverlayBox);

// function initOverlayBox() {
// 	document.querySelector(".overlay-box .toggle-button").addEventListener("click", function() {
// 		console.log(event);
// 	    event.target.classList.toggle("active");
// 	}, true);
// }

// Needs a polyfill and CBF with that crap.
// let root = document.querySelector('.breadcrumb');
// new IntersectionObserver(function([{intersectionRatio}]) {
//   root.classList.toggle('overflowed-content', intersectionRatio < 1);
// }, {root}).observe(root.lastElementChild);