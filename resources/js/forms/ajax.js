function initialiseAjaxForms() {

    var form = document.querySelector("form.ajax");
    form.addEventListener("submit", function(e) {
        e.preventDefault();
        e.target.classList.add("processing");

        var action = this.getAttribute("action");
        var formData = new FormData();
        formData.append("_token", e.target._token.value);
        formData.append("name", e.target.name.value);
        formData.append("current_url", e.target.current_url.value);
        formData.append("report", e.target.report.value);
        formData.append("file", e.target.file.files[0]);
        formData.append("sysinfo", e.target.sysinfo.value);

        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", function () {
            if (this.readyState === 4) {
                resp = JSON.parse(this.responseText);
                if (resp.success) {
                    form.classList.add("hidden");
                    document.querySelector(".bug-reporter .success").classList.remove("hidden");
                    form.classList.remove("processing");
                    form.querySelector("textarea[name='report']").value = "";
                }
            }
        });

        xhr.open("POST", action);
        xhr.send(formData);
        return false;
    });
}
window.addEventListener("load", initialiseAjaxForms);