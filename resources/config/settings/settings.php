<?php
return [
    "colours" => [
        "type"   => "anomaly.field_type.repeater",
        "config" => [
            "related"        => "repeater::theme_colours",
            "add_row"        => "anomaly.field_type.repeater::button.add_row",
        ]
    ],
];