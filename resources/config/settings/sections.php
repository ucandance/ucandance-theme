<?php

return [
	"theme" => [
		"context" => "primary",
		"title" => "finnito.theme.ucandance::label.theme",
		"fields" => [
			"logo",
		],
	],
];