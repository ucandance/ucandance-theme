var postcss = require('gulp-postcss');
var gulp = require('gulp');
var autoprefixer = require('autoprefixer');
var cssnano = require('cssnano');
var sass = require('gulp-sass');

gulp.task('default', function () {
    var plugins = [
        autoprefixer({browsers: ['> 5%']}),
        cssnano()
    ];
    return gulp.src('./resources/scss/theme/theme.scss')
    	.pipe(sass())
        .pipe(postcss(plugins))
        .pipe(gulp.dest('./resources/dist'));
});

gulp.task("watch", function () {
	gulp.watch("./resources/scss/theme/*.scss", gulp.series("default"));
	gulp.watch("./resources/scss/theme/component/*.scss", gulp.series("default"));
    gulp.watch("./resources/scss/theme/layout/*.scss", gulp.series("default"));
    gulp.watch("./resources/scss/theme/module/*.scss", gulp.series("default"));
    gulp.watch("./resources/scss/theme/partial/*.scss", gulp.series("default"));
});

