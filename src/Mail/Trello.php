<?php namespace Finnito\UcandanceTheme\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Trello extends Mailable
{
    use Queueable, SerializesModels;

    public $report;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($report)
    {
        $this->report = $report;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // Has attachment
        if ($this->report->file) {
            return $this->from("bugreport@finnito.nz")
                ->subject($this->report->report)
                ->attach(public_path()."/app/ucandance/files-module/local/".$this->report->file->path())
                ->view('finnito.theme.ucandance::mail.trello')
                ->text('finnito.theme.ucandance::mail.trello');
        }

        // No attachment
        else {
            return $this->from("bugreport@finnito.nz")
                ->subject($this->report->report)
                ->view('finnito.theme.ucandance::mail.trello')
                ->text('finnito.theme.ucandance::mail.trello');
        }
    }
}
