<?php namespace Finnito\UcandanceTheme\Form;

use Anomaly\Streams\Platform\Support\Collection;
use Anomaly\Streams\Platform\Ui\Form\FormBuilder;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Finnito\UcandanceTheme\Mail\Trello;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Response;

class ReportFormHandler
{

    use DispatchesJobs;

    public function handle(FormBuilder $builder)
    {
        $vals = $builder->getFormValues();
        // dd();
        Mail::to("finnlesueur+ku05cbxs37a3yfgbrlje@boards.trello.com")
            ->send(new Trello($vals));
    }
}
